Role Name
=========

Setup-nginx-proxy

Requirements
------------

Before executing the playbook, make sure all the dependency packages are installed.

Role Variables
--------------

All the variables are defined in the defaults/main.yml

- nginx_ssl_certificate_subject: Certificate subject
- nginx_ssl_certificate_days: Certificate validity (days)
- nginx_ssl_certificate: Server path to SSL certificate
- nginx_ssl_certificate_key: Server path to SSL certificate key

Dependencies
------------

- setup-tomcat

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: setup-nginx-proxy}
