Role Name
=========

deploy-web-app

Requirements
------------

Before executing this playbook, please make sure that all the dependency packages are in place.

Role Variables
--------------

All the role variables are defined in the defaults/main.yml

- warName: Application war file name
- warRemotePath: Remote download path
- warROOTDir : Application Root path

Dependencies
------------

- setup-tomcat Playbook
- setup-nginx Playbook

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: deploy-app }
