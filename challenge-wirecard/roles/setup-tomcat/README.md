Role Name
=========

Setup-tomcat

Requirements
------------

Before executing this playbook, please make sure all the dependency packages are installed.

Role Variables
--------------

All the variables are defined in the defaults/main.yml

- java_package: java package name
- http_port: tomcat http application port
- https_port: tomcat https application port
- tomcat_package_url: tomcat package download url
- tomcat_package_name: tomcat package name
- admin_username: tomcat manager-gui username
- admin_password: tomcat manager-gui password

Dependencies
------------

- java

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: setup-tomcat }
