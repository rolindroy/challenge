# Web Application Deployment using Ansible

This project will have the Ansible playbook which can be used to setup an infrastructure and deploying the web application.

This project aims to cover process of DevOps Lifecycle of continues integration that starts from "setting up the infrastructure" to "Deploy and release" the Application.


Currently the project support only CentOS/RedHat environments.
It will setup and configure the following on an CentOS 7.5 or latest.

  - Spin up a Tomcat WebServer
  - Setup and Configure Reverse Proxy With self signed SSL
  - Deploy the Web Application
  - Verify the Deployment

## Requirements

This playbook requires Ansible 2.4 or higher and CentOS/RedHat 7.5 or latest.

## Dependencies

None

## Role Variables

### Role: `setup-tomcat`


```yml
# Java package name
java_package: java-1.8.0-openjdk

# Tomcat http application port
http_port: 8080

# Tomcat https application port
https_port: 8443

# Tomcat package download url
tomcat_package_url: https://www-us.apache.org/dist/tomcat/tomcat-9/v9.0.14/bin/apache-tomcat-9.0.14.tar.gz

# Tomcat package name
tomcat_package_name: apache-tomcat-9.0.14

# This will configure a default manager-gui user:
admin_username: admin
admin_password: password
```

### Role: `setup-nginx-proxy`


```yml
# Certificate subject
nginx_ssl_certificate_subject: "/C=UK/ST=Scotland/L=Dundee/O=OME/CN={{ ansible_fqdn }}"

# Certificate validity (days)
nginx_ssl_certificate_days: 365

# Server path to SSL certificate
nginx_ssl_certificate: /etc/nginx/ssl/server.crt

# Server path to SSL certificate key
nginx_ssl_certificate_key: /etc/nginx/ssl/server.key

```

### Role: `deploy-web-app`

```yml
# Application war file name
war_name: devopschallenge.war

# Temp remote location to download the war file
war_remote_path: /tmp

# Application Root path
war_root_dir: /usr/local/tomcat9/webapps/ROOT
```
### Inventory

```yml
[all]
Server ansible_ssh_user='username' ansible_ssh_private_key_file=<private key path>
```

## How to execute
 Playbook can be execited by running following command

  ```ansible-playbook -i host webapp-playbook.yml --tags setup```
  
  tags: 

1. ```setup``` : Install and configure all the dependancy packages and deploy web application
2. ```deploy``` : Deploy the Web Application
  
### Example Playbook

```yml
- name: Tomcat Application Stack
  hosts: all
  become: yes
  become_method: sudo

  roles:
    - role: setup-tomcat
      tags: [ 'setup' ]

    - role: setup-nginx-proxy
      tags: [ 'setup' ]

    - role: deploy-web-app
      tags: [ 'setup', 'deploy' ]

```
  Once the playbook is executed, will be able to access the server by using following link.

  https://<IP_Address>/hello		

![Example: Running Application](https://bitbucket.org/rolindroy/challenge/raw/ff560d43cd645c30684cdbe15ad5510a3ea80396/challenge-wirecard/output.png)


### Test Cases

The playbook can be tested by using vagrant.

1. ```vagrant up``` : This command creates and configures guest machines according to the Vagrantfile.
2. ```vagrant provision``` : Runs the configured provisioners (Playbook) against the running Vagrant managed machine.

## License

None

## Author Information

This project was created in 2018 by Rolind Roy
